﻿using System;
using System.Windows.Forms;

namespace Oef_15._1_29052020
{
    public partial class Form1 : Form
    {
        Teller t1 = new Teller();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnLezen_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Waarde teller: " + t1.Tel, "Waarde Teller", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }
        private void btnResetten_Click(object sender, EventArgs e)
        {
            //methoden resetten
            MessageBox.Show("Teller resetten?", "Teller Resetten", MessageBoxButtons.OK, MessageBoxIcon.Error);
            t1.Resetten();
        }

        private void btnVerhoog1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Wilt u de teller verhogen? " + t1.Tel, "Teller verhogen", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                t1.Verhoog();
            }

        }
        private void btnEnd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ben je zeker dat je wilt afsluiten?", "Afsluiten", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnVerminder1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Wilt u de teller verlagen? " + t1.Tel, "Teller verlagen", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                t1.Verlagen();
            }
        }
    }
}

