﻿namespace Oef_15._1_29052020
{
    class Teller
    {
        //Attributen, velden, eigenschappen, …
        private int _tel;

        //Constructor
        public Teller()
        {
            this._tel = 0;
        }

        //Properties
        public int Tel
        {
            get { return _tel; }
            set { _tel = value; }
        }

        //Methoden
        public void Resetten()
        {
            _tel = 0;
        }
        public void Verhoog()
        {
            _tel += 1;
        }
        public void Verlagen()
        {
            _tel -= 1;
        }
    }
}
