﻿namespace Oef_15._1_29052020
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLezen = new System.Windows.Forms.Button();
            this.btnVerhoog1 = new System.Windows.Forms.Button();
            this.btnVerminder1 = new System.Windows.Forms.Button();
            this.btnResetten = new System.Windows.Forms.Button();
            this.btnEnd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLezen
            // 
            this.btnLezen.Location = new System.Drawing.Point(11, 16);
            this.btnLezen.Name = "btnLezen";
            this.btnLezen.Size = new System.Drawing.Size(225, 38);
            this.btnLezen.TabIndex = 0;
            this.btnLezen.Text = "Lezen waarde teller";
            this.btnLezen.UseVisualStyleBackColor = true;
            this.btnLezen.Click += new System.EventHandler(this.btnLezen_Click);
            // 
            // btnVerhoog1
            // 
            this.btnVerhoog1.Location = new System.Drawing.Point(11, 60);
            this.btnVerhoog1.Name = "btnVerhoog1";
            this.btnVerhoog1.Size = new System.Drawing.Size(225, 38);
            this.btnVerhoog1.TabIndex = 1;
            this.btnVerhoog1.Text = "Verhoog teller met 1";
            this.btnVerhoog1.UseVisualStyleBackColor = true;
            this.btnVerhoog1.Click += new System.EventHandler(this.btnVerhoog1_Click);
            // 
            // btnVerminder1
            // 
            this.btnVerminder1.Location = new System.Drawing.Point(12, 104);
            this.btnVerminder1.Name = "btnVerminder1";
            this.btnVerminder1.Size = new System.Drawing.Size(225, 38);
            this.btnVerminder1.TabIndex = 2;
            this.btnVerminder1.Text = "Verminder teller met 1";
            this.btnVerminder1.UseVisualStyleBackColor = true;
            this.btnVerminder1.Click += new System.EventHandler(this.btnVerminder1_Click);
            // 
            // btnResetten
            // 
            this.btnResetten.Location = new System.Drawing.Point(12, 148);
            this.btnResetten.Name = "btnResetten";
            this.btnResetten.Size = new System.Drawing.Size(225, 38);
            this.btnResetten.TabIndex = 3;
            this.btnResetten.Text = "Resetten teller naar 0";
            this.btnResetten.UseVisualStyleBackColor = true;
            this.btnResetten.Click += new System.EventHandler(this.btnResetten_Click);
            // 
            // btnEnd
            // 
            this.btnEnd.Location = new System.Drawing.Point(12, 192);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(225, 38);
            this.btnEnd.TabIndex = 4;
            this.btnEnd.Text = "END";
            this.btnEnd.UseVisualStyleBackColor = true;
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 245);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.btnResetten);
            this.Controls.Add(this.btnVerminder1);
            this.Controls.Add(this.btnVerhoog1);
            this.Controls.Add(this.btnLezen);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLezen;
        private System.Windows.Forms.Button btnVerhoog1;
        private System.Windows.Forms.Button btnVerminder1;
        private System.Windows.Forms.Button btnResetten;
        private System.Windows.Forms.Button btnEnd;
    }
}

